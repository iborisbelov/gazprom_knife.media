;document.addEventListener('DOMContentLoaded', () => {

	window.DEV = false

	// animal animation
	const animalAnimationFn = {
		tulen: () => {
			const $animalWrap = $('.animal--tulen')
			const $tulenTulen = $('.tulen-item img.tulen')
			const $helicopterItem = $('.helicopter-item')
			const $helicopterRope = $('.helicopter-item img.rope')

			// helicopter
			$helicopterItem.addClass('moveTo')
			
			setTimeout(() => {
				$helicopterRope.addClass('active')
			}, 3000)
			
			setTimeout(() => {
				$helicopterRope.addClass('active')
				$tulenTulen.addClass('rotate')
			}, 4000)
			
			setTimeout(() => {
				$animalWrap.addClass('moveBack')
			}, 4500)

			setTimeout(() => {
				$helicopterRope.removeClass('active')
				$tulenTulen.addClass('jump')
			}, 7500)

			setTimeout(() => {
				$helicopterItem.addClass('moveAway')
			}, 8500)

			// show win popup
			setTimeout(() => {
				app.showPopupWin = true
			}, 9000)
		},

		kazarka: () => {

			$('.animal--kazarka img.pointer').addClass('active')
			$('.animal--kazarka').addClass('animated')

			// show win popup
			setTimeout(() => {
				app.showPopupWin = true
			}, 4000)
		},

		volk: () => {
			$('.animal--volk').addClass('animated')

			// show win popup
			setTimeout(() => {
				app.showPopupWin = true
			}, 4000)
		},

		olen: () => {
			$('.animal--olen').addClass('animated')

			$('.olen-2').addClass('rotate')
			setTimeout(() => {
				$('.olen-2').addClass('move')
			}, 200)
			
			
			setTimeout(() => {
				$('.olen-3').addClass('rotate')
			}, 1000)

			setTimeout(() => {
				$('.olen-3').addClass('move')
			}, 1200)

			setTimeout(() => {
				$('.olen-4').addClass('rotate')
			}, 2000)

			setTimeout(() => {
				$('.olen-4').addClass('move')
			}, 2200)

			

			// show win popup
			setTimeout(() => {
				app.showPopupWin = true
			}, 5000)
		},

		beluga: () => {
			$('.animal--beluga').addClass('animated')

			
			setTimeout(() => {
				$('img.beluga').addClass('rotate')
			}, 1200)
			setTimeout(() => {
				$('img.beluga').addClass('move')
			}, 1400)

			// show win popup
			setTimeout(() => {
				app.showPopupWin = true
			}, 4500)
		},

		punochka: () => {
			$('img.punochka').addClass('rotate')

			setTimeout(() => {
				$('img.punochka').addClass('move')
			}, 200)

			// show win popup
			setTimeout(() => {
				app.showPopupWin = true
			}, 4000)
		},

		narval: () => {
			setTimeout(() => {
				$('img.treshina-2').addClass('active')
			}, 500)
			setTimeout(() => {
				$('img.treshina-3').addClass('active')
			}, 1000)

			setTimeout(() => {
				$('img.narval').addClass('moveAway')
			}, 2000)

			// show win popup
			setTimeout(() => {
				if (app.currentAnimal.integration) {
					app.showIntegration = true
				} else {
					app.showPopupWin = true
				}
			}, 4000)
		},

		plankton: () => {
			$('.animal--plankton').addClass('animated')

			$('.animal--plankton img.copter').addClass('move')

			setTimeout(() => {
				$('.animal--plankton img.copter').addClass('rotate')
			}, 1000)
			
			setTimeout(() => {
				$('.animal--plankton img.copter').removeClass('move')
			}, 4000)

			// show win popup
			setTimeout(() => {
				if (app.currentAnimal.integration) {
					app.showIntegration = true
				} else {
					app.showPopupWin = true
				}
			}, 6000)
		},

		medved: () => {
			$('.animal--medved').addClass('animated')

			setTimeout(() => {
				$('img.medved').addClass('rotate')
			}, 900)

			setTimeout(() => {
				$('img.medved').addClass('move')
			}, 1000)

			// show win popup
			setTimeout(() => {
				app.showPopupWin = true
			}, 4000)
		},

		riba: () => {
			$('img.vedro').addClass('drop')
			
			setTimeout(() => {
				$('img.riba').addClass('rotate')
			}, 400)
		
			setTimeout(() => {
				$('img.riba').addClass('move')
			}, 600)

			// show win popup
			setTimeout(() => {
				if (app.currentAnimal.integration) {
					app.showIntegration = true
				} else {
					app.showPopupWin = true
				}
			}, 4000)
		}
	}


	const audio = {
		beluga: $('audio.beluga'),
		tulen: $('audio.tulen'),
		kazarka: $('audio.kazarka'),
		volk: $('audio.volk'),
		olen: $('audio.olen'),
		punochka: $('audio.punochka'),
		narval: $('audio.narval'),
		plankton: $('audio.plankton'),
		medved: $('audio.medved'),
		riba: $('audio.riba'),
		click: $('audio.click'),
		achievement: $('audio.achievement'),
		radar: $('audio.radar'),
		successfull: $('audio.successfull'),
		lose: $('audio.lose'),
		soundtrack: $('audio.soundtrack')
	}

	const infoAnimals = {
		tulen: {
			label: 'tulen',
			imgNotify: './img/notify/tulen.png',
			imgSrc: './img/info-animal-1.png',
			isDone: false,
			stars: 0,
			maxStars: 3
		},
		kazarka: {
			label: 'kazarka',
			imgNotify: './img/notify/kazarka.png',
			imgSrc: './img/info-animal-2.png',
			isDone: false,
			stars: 0,
			maxStars: 3
		},
		volk: {
			label: 'volk',
			imgNotify: './img/notify/volk.png',
			imgSrc: './img/info-animal-3.png',
			isDone: false,
			stars: 0,
			maxStars: 3
		},
		olen: {
			label: 'olen',
			imgNotify: './img/notify/olen.png',
			imgSrc: './img/info-animal-4.png',
			isDone: false,
			stars: 0,
			maxStars: 1
		},
		beluga: {
			label: 'beluga',
			imgNotify: './img/notify/beluga.png',
			imgSrc: './img/info-animal-5.png',
			isDone: false,
			stars: 0,
			maxStars: 2
		},
		punochka: {
			label: 'punochka',
			imgNotify: './img/notify/punochka.png',
			imgSrc: './img/info-animal-6.png',
			isDone: false,
			stars: 0,
			maxStars: 2
		},
		narval: {
			label: 'narval',
			imgNotify: './img/notify/narval.png',
			imgSrc: './img/info-animal-7.png',
			isDone: false,
			stars: 0,
			maxStars: 2
		},
		plankton: {
			label: 'plankton',
			imgNotify: './img/notify/plankton.png',
			imgSrc: './img/info-animal-8.png',
			isDone: false,
			stars: 0,
			maxStars: 2
		},
		medved: {
			label: 'medved',
			imgNotify: './img/notify/medved.png',
			imgSrc: './img/info-animal-9.png',
			isDone: false,
			stars: 0,
			maxStars: 2
		},
		riba: {
			label: 'riba',
			imgSrc: './img/info-animal-10.png',
			isDone: false,
			stars: 0,
			maxStars: 2
		}
	}


	const animals = [
		{
			label: 'tulen',
			name: 'Тюлень',
			infoNotify: 'Гренландский тюлень на льдине уплывает слишком далеко от берега',
			animalNotify: 'Извини, немного перетюленился',
			radar: './img/radar/top.svg',
			win: {
				title: 'Смотритель маяка',
				achivmentText: 'Вы помогли всем гренландским тюленям вернуться в гавань',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'kazarka',
			name: 'Казарка',
			infoNotify: 'Казарки потеряли друг друга во льдах',
			animalNotify: 'Между нами дверь стеклянная, между нами тишина',
			radar: './img/radar/right-top.svg',
			win: {
				title: 'Арктический Купидон',
				achivmentText: 'Вы помогли всем казаркам найти друг друга',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'plankton',
			name: 'Планктон',
			infoNotify: 'Попробуйте отыскать нас!',
			animalNotify: 'Ребята, плывем на юг без остановки с 10 до 19, никаких перерывов на обед',
			radar: './img/radar/right.svg',
			win: {
				title: 'Полярный друг',
				achivmentText: 'Вы указали все планктоновые островки в море',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...',
			integration: 'Внимательность – это главное качество исследователя! <br>«Газпром нефть» <a href="https://shelf.gazprom-neft.ru/press-center/news/64821/" target="_blank">заботится</a> о сохранении биологического разнообразия в юго-восточной части Баренцева моря, где расположена платформа «Приразломная». Например, последние наблюдения зафиксировали сохранение популяции краснокнижных атлантических моржей и развитие фитопланктона. Это свидетельствует об отсутствии неблагоприятного воздействия на морскую экосистему в регионе.'
		},
		{
			label: 'riba',
			name: 'Кета',
			infoNotify: 'Пополните запасы рыбы в море',
			animalNotify: 'То что рыбы не умеют говорить — это миф. Просто было не с кем. Спасибо за компанию!',
			radar: './img/radar/right-top.svg',
			win: {
				title: 'Полярный Нептун',
				achivmentText: 'Вы увеличили популяцию рыбы в реках и море',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'olen',
			name: 'Северные олени',
			infoNotify: 'Покажи нам, где переход!',
			animalNotify: 'Маршрут построен. Следующая остановка — ягельные поля',
			radar: './img/radar/right.svg',
			win: {
				title: 'Потомственный оленевод',
				achivmentText: 'Вы провели всех оленей через переход',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'volk',
			name: 'Волк',
			infoNotify: 'Кажется, я заблудился!',
			animalNotify: 'Безумно можно быть первым волком, который добрался так далеко!',
			radar: './img/radar/left.svg',
			win: {
				title: 'Вожак стаи',
				achivmentText: 'Вы указали путь всем волчатам',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'beluga',
			name: 'Белуга',
			infoNotify: 'Мне надо выбраться изо льдов',
			animalNotify: 'Давай без шейминга, но тут для меня довольно тесно',
			radar: './img/radar/left.svg',
			win: {
				title: 'Атомный ледокол',
				achivmentText: 'Вы помогли всем белухам выбраться из ледяного плена',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'riba',
			name: 'Кета',
			infoNotify: 'Пополните запасы рыбы в море',
			animalNotify: 'Как же они подросли! Помню их еще икринками',
			radar: './img/radar/right-top.svg',
			win: {
				title: 'Полярный Нептун',
				achivmentText: 'Вы увеличили популяцию рыбы в реках и море',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...',
			integration: 'Вы сделали важное дело! Теперь рыбы будет много и даже моржи скажут вам спасибо, ведь им будет чем подкрепиться на следующий год. <br>Еще важнее заботиться о сохранении популяции рыб в реальности. У «Газпром нефти» есть программа восполнения водных биоресурсов. К примеру, в 2020-м году по заказу компании в реки на Камчатке и в Архангельской области <a href="https://shelf.gazprom-neft.ru/press-center/news/65795/" target="_blank">выпустили свыше 230 тысяч мальков кеты</a> и атлантического лосося, выращенных на рыбоводных заводах.'
		},
		{
			label: 'plankton',
			name: 'Планктон',
			infoNotify: 'Попробуйте отыскать нас!',
			animalNotify: 'Кучнее! Покажем, что такое тимбилдинг по-северному',
			radar: './img/radar/right.svg',
			win: {
				title: 'Полярный друг',
				achivmentText: 'Вы указали все планктоновые островки в море',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'punochka',
			name: 'Пуночка',
			infoNotify: 'Куда приземляться?',
			animalNotify: 'Редкая птица долетит до середины Баренцева моря, а потому и мне бы уже отдохнуть',
			radar: './img/radar/right-top.svg',
			win: {
				title: 'Снежный подорожник',
				achivmentText: 'Вы помогли всем пуночкам найти места для гнездования',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'narval',
			name: 'Нарвал',
			infoNotify: 'Я немного застрял',
			animalNotify: 'Я как единорог, только ем не радугу, а ракообразных',
			radar: './img/radar/right-top.svg',
			win: {
				title: 'Морской рыцарь',
				achivmentText: 'Вы помогли всем нарвалам преодолеть препятствия',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'medved',
			name: 'Белый медведь',
			infoNotify: 'я почти у вас в гостях',
			animalNotify: 'Я доберусь до тебя раньше, чем у тебя сядет батарейка',
			radar: './img/radar/center.svg',
			win: {
				title: 'Северный егерь',
				achivmentText: 'Вы смогли увести от лагеря всех белых медведей',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'tulen',
			name: 'Тюлень',
			infoNotify: 'Гренландский тюлень на льдине уплывает слишком далеко от берега',
			animalNotify: 'Ит’c тююлеееейт аполоджайс',
			radar: './img/radar/top.svg',
			win: {
				title: 'Смотритель маяка',
				achivmentText: 'Вы помогли всем гренландским тюленям вернуться в гавань',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'kazarka',
			name: 'Казарка',
			infoNotify: 'Казарки потеряли друг друга во льдах',
			animalNotify: 'Вот бы уже свить семейное гнездышко',
			radar: './img/radar/right-top.svg',
			win: {
				title: 'Арктический Купидон',
				achivmentText: 'Вы помогли всем казаркам найти друг друга',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'narval',
			name: 'Нарвал',
			infoNotify: 'Я немного застрял',
			animalNotify: 'С двухметровым бивнем легко соблюдать социальную дистанцию',
			radar: './img/radar/right-top.svg',
			win: {
				title: 'Морской рыцарь',
				achivmentText: 'Вы помогли всем нарвалам преодолеть препятствия',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...',
			integration: 'Ура, свобода! Теперь нарвал – арктический морской единорог – может присоединиться к своим сородичам. <br>В 2021 году «Газпром нефть» запустила вторую научно-исследовательскую экспедицию, чтобы исследовать популяцию нарвалов, белух и гренландских китов. На основе данных двух экспедиций будет создана научная программа по сохранению нарвала и среды его обитания. Следить за экспедицией можно <a href="https://www.instagram.com/narwhal.ru/" target="_blank">здесь</a>.'
		},
		{
			label: 'volk',
			name: 'Волк',
			infoNotify: 'Кажется, я заблудился!',
			animalNotify: 'Я смелее льва и тигра, но, кажется, я опять потерялся',
			radar: './img/radar/left.svg',
			win: {
				title: 'Вожак стаи',
				achivmentText: 'Вы указали путь всем волчатам',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'kazarka',
			name: 'Казарка',
			infoNotify: 'Казарки потеряли друг друга во льдах',
			animalNotify: 'А я про всё на свете с тобою забываю, а я в любовь, как в море, бросаюсь с головой',
			radar: './img/radar/right-top.svg',
			win: {
				title: 'Арктический Купидон',
				achivmentText: 'Вы помогли всем казаркам найти друг друга',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'beluga',
			name: 'Белуга',
			infoNotify: 'Мне надо выбраться изо льдов',
			animalNotify: 'В следующий раз закажу заводь на пару размеров больше',
			radar: './img/radar/left.svg',
			win: {
				title: 'Атомный ледокол',
				achivmentText: 'Вы помогли всем белухам выбраться из ледяного плена',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'tulen',
			name: 'Тюлень',
			infoNotify: 'Гренландский тюлень на льдине уплывает слишком далеко от берега',
			animalNotify: 'Дрейфуем, сегодня мы с тобой дрейфуем!',
			radar: './img/radar/top.svg',
			win: {
				title: 'Смотритель маяка',
				achivmentText: 'Вы помогли всем гренландским тюленям вернуться в гавань',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'volk',
			name: 'Волк',
			infoNotify: 'Кажется, я заблудился!',
			animalNotify: 'Как же мощны мои лапищи, но они вновь привели меня не туда',
			radar: './img/radar/left.svg',
			win: {
				title: 'Вожак стаи',
				achivmentText: 'Вы указали путь всем волчатам',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'punochka',
			name: 'Пуночка',
			infoNotify: 'Куда приземляться?',
			animalNotify: 'Этим маленьким уставшим крыльям очень нужно в птичье SPA',
			radar: './img/radar/right-top.svg',
			win: {
				title: 'Снежный подорожник',
				achivmentText: 'Вы помогли всем пуночкам найти места для гнездования',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
		{
			label: 'medved',
			name: 'Белый медведь',
			infoNotify: 'Я почти у вас в гостях',
			animalNotify: 'Я, конечно, хищник, но перекусить пару пластмассовых винтов готов',
			radar: './img/radar/center.svg',
			win: {
				title: 'Северный егерь',
				achivmentText: 'Вы смогли увести от лагеря всех белых медведей',
				text: 'Текст без ачивки...'
			},
			lose: 'вы не успели за время бла...'
		},
	]

	window.app = new Vue({
		el: '#app',
		data: {
			timer: null,
			timerTime: 30000, // change to 30000
			muted: false,
			startScreenShow: true,
			isStart: false,
			showEnd: false,
			currentAnimalIndex: 0, // change to 0
			infoAnimals: infoAnimals,
			animals: animals,
			completedTasks: 0,
			showPopupWin: false,
			showPopupLose: false,
			showedPopupLose: false,
			showRules: false,
			stepRules: 1,
			showIntegration: false,
			resultTextCurrent: 'veryGood',
			resultText: {
				veryGood: {
					stars: 5,
					title: 'Вы — Крузенштерн!',
					text: 'С таким капитаном не пропадешь в полярной экспедиции. Видно, вы готовы зимовать на льдине и управлять оленями в упряжке, а в роду у вас были великие мореплаватели, не иначе. <b>Не человек, а ледокол!</b>',
				},
				good: {
					stars: 4,
					title: 'Вы — медвежонок Умка',
					text: 'Вы плывете на льдине, как на бригантине, и вечно ищете приключения. Ваш козырь — любознательность. Вы открыты новому, не боитесь ошибаться и знаете, что найдете друзей даже на Северном полюсе.'
				},
				bad: {
					stars: 3,
					title: 'Вы — снежный человек',
					text: 'Хорошая попытка, но ва чего-то испугались и удрали в тундру. Охотник из вас неплохой, а вот исследователь — так себе. Помните, что на Севере опасно быть дикарем. <b>В следующий раз действуй разумнее!</b>'
				},
				veryBad: {
					stars: 2,
					title: 'Вы — рыбка Дори',
					text: 'Вам бы плавать где-нибудь в Калифорнийском заливе, а не среди арктических льдов. Попробуйте сыграть еще раз, только не забудьте изучить правила!'
				}
			}
		},
		methods: {
			start() {
				this.playSound('soundtrack')
				$('.start-scene').removeClass('animation')
				$('.start-scene .loader-wrap').addClass('active')

				setTimeout(() => {
					$('.start-scene .loader-wrap').removeClass('active')
				}, 4000)

				// show rules
				setTimeout(() => {
					this.startScreenShow = false
				}, 5000)

				setTimeout(() => {
					this.showRules = true
				}, 6000)
			},

			startTimer() {
				$('.timer-indicator').removeClass('paused');

				// start timer
				this.timer = setTimeout(() => {
					this.stopTimer()

					if (!this.showedPopupLose) {
						this.showPopupLose = true // show lose popup
						this.showedPopupLose = true
					} else {
						this.nextAnimal('lose')
					}

					
				}, this.timerTime)
			},

			stopTimer() {
				clearTimeout(this.timer)
				$('.timer-indicator').addClass('paused');
			},

			nextAnimal(result) {
				this.showPopupWin = false
				this.showPopupLose = false
				$(`.animal .notify-wrap`).show()

				if (result === 'win') {
					this.completedTasks++
				}

				// result text
				if (this.completedTasks < 6) {
					this.resultTextCurrent = 'veryBad'
				}
				if (this.completedTasks >= 6) {
					this.resultTextCurrent = 'bad'
				}
				if (this.completedTasks >= 10) {
					this.resultTextCurrent = 'good'
				}
				if (this.completedTasks >= 18) {
					this.resultTextCurrent = 'veryGood'
				}

				// end game
				if (this.currentAnimalIndex >= this.animals.length - 1) {
					this.showEnd = true
					this.stopTimer()
					return
				}

				this.currentAnimalIndex++

				this.startTimer()

				this.showTabloNotify()

				this.playSound('radar')
			},

			playSound(sound){
				audio[sound].trigger('play')

				if (sound === 'radar') {
					$('.compas-wrap img').addClass('active')
					setTimeout(() => {
						$('.compas-wrap img').removeClass('active')
					}, 1500)
				}
			},

			animalAnimation(animal) {
				this.stopTimer()
				
				this.playSound(animal)
				$(`.animal--${animal} .notify-wrap`).fadeOut(250)
				$(`.animal--${animal}`).addClass('pin')
				
				// run animal animation
				animalAnimationFn[animal]()
			},

			showTabloNotify(){
				setTimeout(() => {
					$(this.$refs.animalNotify).fadeIn(500).delay(5000).fadeOut(500)
				}, 0)
			},

			nextRules(){
				if (this.stepRules === 3) {
					this.stepRules = 1
					this.showRules = false

					// start
					if (!this.isStart) {
						this.isStart = true
						this.startTimer()
						this.showTabloNotify()
						this.playSound('radar')
					}
					return
				}

				this.stepRules++
			}
		},
		computed: {
			currentAnimal(){
				return this.animals[this.currentAnimalIndex]
			},
			currentAnimalStars(){
				return this.infoAnimals[this.currentAnimal.label].stars
			},
			currentAnimalMaxStars(){
				return (this.infoAnimals[this.currentAnimal.label].stars >= this.infoAnimals[this.currentAnimal.label].maxStars)
			}
		},
		watch: {
			showPopupWin() {
				if (this.showPopupWin) {
					// add animal star
					this.infoAnimals[this.currentAnimal.label].stars++

					if (this.currentAnimalMaxStars) {
						this.playSound('achievement')
					} else {
						this.playSound('successfull')
					}
				}
			},
			showPopupLose() {
				if (this.showPopupLose) {
					this.playSound('lose')

					window.setCenterMap()
				}
			},
			showEnd() {
				if (this.showEnd) {
					this.playSound('successfull')
					setTimeout(() => {
						$('.end-scene').addClass('animation')
					}, 0)
				}
			},
			muted(){
				if (this.muted) {
					$('audio').prop('volume', 0)
				} else {
					$('audio').prop('volume', 1)
				}
			}
		},
		mounted(){
			initMap() // map

			setTimeout(() => {
				$('body').removeClass('hidden')
				
				// start animation
				$('.start-scene').addClass('animation')
			}, 500)


			// DEV
			if (DEV) {
				this.startScreenShow = false
				this.isStart = true
				this.startTimer()
				this.showTabloNotify()
				this.playSound('radar')
			}
		}
	})

});



function initMap(){
	// move map
	const drag = Draggable.create('.map-wrapper', {
		bounds: '#map-container',
		edgeResistance: 1,
		type: "x,y"
	})[0]

	// drag.addEventListener('dragend', function(e){
	// 	console.log('qweqwe', drag);
	// })


	// center map
	window.setCenterMap = function(){
		const windowInnerWidth = window.innerWidth / 2;
		const windowInnerHeight = window.innerHeight / 2;

		const halfCordX = $('.map-scene .map-wrapper').width() / 2
		const halfCordY = $('.map-scene .map-wrapper').height() / 1.3
		const cordX = Math.round(halfCordX - windowInnerWidth)
		const cordY = Math.round(halfCordY - windowInnerHeight)
		const cordXm = '-' + cordX
		const cordYm = '-' + cordY

		TweenLite.set('.map-wrapper', {x:cordXm, y:cordYm})
	}

	setCenterMap()

	// water animation
	setInterval(function() {
		$('.water-block').toggleClass('rotate')
	}, 500)
}


$(window).on('load resize', () => {
	let vh = window.innerHeight * 0.01;
	document.documentElement.style.setProperty('--vh', `${vh}px`);
});


